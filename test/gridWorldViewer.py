import unittest

import src.gridWorldViewer as viewer
from src.env import Environment
from src.policyIteration import PolicyIteration

class TestCase(unittest.TestCase):
	def test_print(self):
		env = Environment(5,5)
		env.setReward((2,1),-1)
		env.setReward((1,2),-1)
		env.setReward((2,2), 1)
		
		pi = PolicyIteration(env)
		pi.policyEvaluation()
		pi.policyImprovement()
		viewer.printWorld(env, (0,0), pi.valueTable, pi.policyTable,"./sample/sample.png")