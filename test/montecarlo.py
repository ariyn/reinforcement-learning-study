import unittest

import src.gridWorldViewer as viewer
from src.env import Environment
from src.montecarlo import Montecarlo

class TestCase(unittest.TestCase):
	def test_valueTable(self):
		env = Environment(5,5)
		env.setReward((2,1),-1)
		env.setReward((1,2),-1)
		env.setReward((2,2), 1)
		
		monte = Montecarlo(env)
		for i in range(30):
			monte.episode()
		viewer.printWorld(env, (0,0), monte.valueTable, [],"./sample/montecarlo30.png")
		
		for i in range(470):
			monte.episode()
		viewer.printWorld(env, (0,0), monte.valueTable, [],"./sample/montecarlo500.png")
		
		for i in range(500):
			monte.episode()
		viewer.printWorld(env, (0,0), monte.valueTable, [],"./sample/montecarlo1000.png")
		
		for i in range(2000):
			monte.episode()
		viewer.printWorld(env, (0,0), monte.valueTable, [],"./sample/montecarlo3000.png")
		