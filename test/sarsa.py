import unittest

import src.gridWorldViewer as viewer
from src.env import Environment
from src.sarsa import Sarsa

class TestCase(unittest.TestCase):
	def test_qTable(self):
		env = Environment(5,5)
		env.setReward((2,1),-100)
		env.setReward((1,2),-100)
		env.setReward((2,2), 100)
		
		sarsa = Sarsa(env)
		sarsa.maximum = 1
		
		for i in range(1000):
			sarsa.episode()
		
		qTable = [list(i.values()) for i in sarsa.qTable.values()]
		viewer.printWorld(env, (0,0), [], qTable,"./sample/sarsa1000.png", policyNumber=True)