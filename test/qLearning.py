import unittest

import src.gridWorldViewer as viewer
from src.env import Environment
from src.qLearning import qLearning

class TestCase(unittest.TestCase):
	def test_qTable(self):
		env = Environment(5,5)
		env.setReward((2,1),-100)
		env.setReward((1,2),-100)
		env.setReward((2,2), 100)
		
		ql = qLearning(env)
		
		for i in range(1000):
			ql.episode()
		
		qTable = [list(i.values()) for i in ql.qTable.values()]
		viewer.printWorld(env, (0,0), [], qTable,"./sample/qLearning1000.png", policyNumber=True)
    
		for i in range(3000):
			ql.episode()
		
		qTable = [list(i.values()) for i in ql.qTable.values()]
		viewer.printWorld(env, (0,0), [], qTable,"./sample/qLearning4000.png", policyNumber=True)
		
		for i in range(2000):
			ql.episode()
		
		qTable = [list(i.values()) for i in ql.qTable.values()]
		viewer.printWorld(env, (0,0), [], qTable,"./sample/qLearning6000.png", policyNumber=True)