import unittest

import src.gridWorldViewer as viewer
from src.env import Environment
from src.valueIteration import ValueIteration

def getPolicyTable(env, pi):
	policyTable = []
	for y in range(env.height):
		policyTable.append([])
		for x in range(env.width):
			actions = pi.getAction((y,x))
			policy = [0,0,0,0]
			for i in range(4):
				if i in actions:
					policy[i] = 1
			policyTable[y].append(policy)
	return policyTable

class TestCase(unittest.TestCase):
	def test_valueTable(self):
		env = Environment(5,5)
		env.setReward((2,1),-1)
		env.setReward((1,2),-1)
		env.setReward((2,2), 1)
		
		pi = ValueIteration(env)
		pi.valueIteration()
		policyTable = getPolicyTable(env, pi)
		viewer.printWorld(env, (0,0), pi.valueTable, policyTable,"./sample/valueIterationSample.png")
		
		for i in range(30):
			pi.valueIteration()
		policyTable = getPolicyTable(env, pi)
		viewer.printWorld(env, (0,0), pi.valueTable, policyTable,"./sample/valueIterationSample30.png")