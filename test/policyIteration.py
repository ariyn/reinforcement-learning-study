import unittest

from src.env import Environment
from src.policyIteration import PolicyIteration

class TestCase(unittest.TestCase):
	def test_valueTable(self):
		env = Environment(5,5)
		env.setReward((2,1),-1)
		env.setReward((1,2),-1)
		env.setReward((2,2), 1)
		
		pi = PolicyIteration(env)
		pi.policyEvaluation()
		self.assertEqual(pi.valueTable[1][1], -0.5)
		self.assertEqual(pi.valueTable[1][2], 0.25)
		