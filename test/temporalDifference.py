import unittest

import src.gridWorldViewer as viewer
from src.env import Environment
from src.temporalDifference import TemporalDifference

class TestCase(unittest.TestCase):
	def test_valueTable(self):
		env = Environment(5,5)
		env.setReward((2,1),-1)
		env.setReward((1,2),-1)
		env.setReward((2,2), 1)
		
		td = TemporalDifference(env)
		td.maximum = 1
		
		for i in range(9000):
			td.episode()
		viewer.printWorld(env, (0,0), td.valueTable, [],"./sample/tempo5000.png")