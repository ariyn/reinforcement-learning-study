import unittest

from src.env import Environment

class TestCase(unittest.TestCase):
	def setUp(self):
		self.env = Environment(5,5)
		
	def test_checkStates(self):
		env = self.env
		env.states[0][0] = 1
		self.assertNotEqual(env.states[0][0], env.states[1][0])
	
	def test_checkStateLists(self):
		states = self.env.getAllStates()
		self.assertEqual(states[0], (0,0))
		self.assertEqual(states[4], (0,4))
		
	def test_getStateAction(self):
		newState = self.env.getStateAction((1,1), 0)
		self.assertEqual(newState, (0,1))
		newState = self.env.getStateAction((1,1), 1)
		self.assertEqual(newState, (1,0))
		newState = self.env.getStateAction((1,1), 2)
		self.assertEqual(newState, (2,1))
		newState = self.env.getStateAction((1,1), 3)
		self.assertEqual(newState, (1,2))
		
		newState = self.env.getStateAction((0,0), 0)
		self.assertEqual(newState, (0,0))
		newState = self.env.getStateAction((0,0), 1)
		self.assertEqual(newState, (0,0))
		newState = self.env.getStateAction((4,4), 3)
		self.assertEqual(newState, (4,4))
		newState = self.env.getStateAction((4,4), 2)
		self.assertEqual(newState, (4,4))
	
	def test_setReward(self):
		self.env.setReward((0,0), 1)
		reward = self.env.getReward((0,0))
		self.assertEqual(reward, 1)
		
		self.env.setReward((0,0), -1)
		reward = self.env.getReward((0,0))
		self.assertEqual(reward, -1)