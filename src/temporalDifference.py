
from .montecarlo import Montecarlo
class TemporalDifference(Montecarlo):
	def __init__(self, env):
		super(TemporalDifference, self).__init__(env)

	def update(self, sample):
		state, action, reward, nextState, done = sample[0]
		ns = nextState
		
# 		print(state, action, reward, nextState)
# 		print(self.valueTable[state[0]][state[1]])
		value = self.valueTable[state[0]][state[1]]
		rv = reward+self.discountFactor*self.valueTable[ns[0]][ns[1]]-value
		self.valueTable[state[0]][state[1]] = value+self.learningRate*rv
		
		if done:
			self.state = (0,0)
		else:
			self.state = nextState