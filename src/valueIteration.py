
class ValueIteration:
	def __init__(self, env):
		self.env = env
		self.valueTable = [[0.00] * env.width for _ in range(env.height)]
		self.discountFactor = 0.9
		
	def valueIteration(self):
		nextValueTable = [[0.00]*self.env.width for _ in range(self.env.height)]
		for state in self.env.getAllStates():
			if state == (2,2):
				nextValueTable[2][2] = 0.0
				continue
			valueList = []
			
			for action in self.env.possibleActions:
				nextState = self.env.getStateAfterAction(state, action)
				reward = self.env.getReward(nextState)
				nextValue = self.getValue(nextState)
				valueList.append(reward+self.discountFactor*nextValue)
			nextValueTable[state[0]][state[1]] = round(max(valueList), 2)
		self.valueTable = nextValueTable
	
	def getAction(self, state):
		actionList = []
		maxValue = -99999
		
		if state == (2,2):
			return []
		
		for action in self.env.possibleActions:
			nextState = self.env.getStateAfterAction(state, action)
			reward = self.env.getReward(nextState)
			nextValue = self.getValue(nextState)
			value = (reward+self.discountFactor*nextValue)
			
			if maxValue < value:
				actionList.clear()
				actionList.append(action)
				maxValue = value
			elif maxValue == value:
				actionList.append(action)
		return actionList
	
	def getValue(self, state):
		return round(self.valueTable[state[0]][state[1]], 2)