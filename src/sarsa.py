from collections import defaultdict
from random import randint, choice

class Sarsa:
	def __init__(self, env):
		self.env = env
		self.actions = [0,1,2,3]
		self.learningRate = 0.01
		self.discountFactor = 0.9
		self.epsilon = 0.1
		self.qTable = defaultdict(lambda:defaultdict(lambda:[0.0,0.0,0.0,0.0]))
		
		self.state = (0,0)
		
	def learn(self, state, action, reward, nextState, nextAction):
		ns, na = nextState, nextAction
		
		oldQ = self.qTable[state[0]][state[1]][action]
		newQ = self.qTable[ns[0]][ns[1]][na]
		
		rq = reward+self.discountFactor*newQ-oldQ
		self.qTable[state[0]][state[1]][action] = oldQ+self.learningRate*rq
	
	def episode(self):
		state = self.state
		
		done = False
		while not done:
			action = self.getAction(state)
			nextState = self.env.getStateAfterAction(state, action)
			reward = self.env.getReward(nextState)
			nextAction = self.getAction(nextState)
			
			if nextState == (2,2):
				done = True
			
			self.learn(state, action, reward, nextState, nextAction)
			
			state = nextState
			action = nextAction
	
	def getAction(self, state):
		if randint(0,1) < self.epsilon:
			action = choice(self.actions)
		else:
			stateAction = self.qTable[state[0]][state[1]]
			action = self.argMax(stateAction)
		return action
	
	def argMax(self, stateAction):
		maxIndex = []
		maxValue = stateAction[0]
		
		for index, value in enumerate(stateAction):
			if maxValue < value :
				maxIndex.clear()
				maxIndex.append(index)
			elif value == maxValue:
				maxIndex.append(index)
		return choice(maxIndex)