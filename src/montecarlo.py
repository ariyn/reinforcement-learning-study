from random import choice, randint

class Montecarlo:
	def __init__(self, env):
		self.env = env
		self.valueTable = [[0.0]*env.width for _ in range(env.height)]
		self.discountFactor = 0.9
		self.learningRate = 0.01
		self.epsilon = 0.1
		self.maximum = 300
		self.state = (0,0)

	def episode(self):
		samples = []
		state = self.state
		
		index = 0
		done = False
		while index < self.maximum and not done:
			action = self.getAction(state)
			nextState = self.env.getStateAfterAction(state, action)
			reward = self.env.getReward(nextState)
			
			if nextState == (2,2):
				done = True
			
			samples.append((state, action, reward, nextState, done))
			state = nextState
			index += 1
		self.update(samples)
	
	def update(self, samples):
		gt = 0
		visitState = []
		for s in samples[::-1]:
			_, _, reward, state, done = s
			if state not in visitState:
				visitState.append(state)
				gt = self.discountFactor * (reward+gt)
				value = self.valueTable[state[0]][state[1]]
# 				print(state, reward, done, gt, value, value+self.learningRate*(gt-value))
				self.valueTable[state[0]][state[1]] = value+self.learningRate*(gt-value)
	
		self.state = (0,0)
			
	def getAction(self, state):
		if randint(0,1) < self.epsilon:
			action = choice([0,1,2,3])
		else:
			nextStates = self.getPossibleNextState(state)
			action = self.argMax(nextStates)
		return action
	
	def argMax(self, states):
		maxIndex = []
		maxValue = states[0]
		for index, value in enumerate(states):
			if maxValue < value:
				maxIndex.clear()
				maxIndex.append(index)
				maxValue = value
			elif maxValue == value:
				maxIndex.append(index)
		return choice(maxIndex)
		
	def getPossibleNextState(self, state):
		y, x = state
		nextStates = [0]*4
		
		if 0 < x: nextStates[1] = self.valueTable[y][x-1]
		if x < self.env.width-1: nextStates[3] = self.valueTable[y][x+1]
		if 0 < y: nextStates[0] = self.valueTable[y-1][x]
		if y < self.env.height-1: nextStates[2] = self.valueTable[y+1][x]
		
		return nextStates