from PIL import Image, ImageDraw, ImageFont

mode = "RGB"
size = (300,300)

def drawTextPolicy(draw, loc, fill, text, left=0, right=0, top=0, bottom=0):
	if left:left=5
	if right:right=5
	if bottom:bottom=0
	if top:top=0
	
	font = ImageFont.truetype("resources/UbuntuMono-R.ttf", 9)
	text = "%0.1f"%text
	size = draw.textsize(text, font=font)

	horMargin = left-right
	verMargin = top-bottom
	
	
	loc = ((loc[2]+loc[0])//2, (loc[3]+loc[1])//2)
	loc = (loc[0]-size[0]/2+horMargin,loc[1]-size[1]/2+verMargin)
	draw.text(loc, text, fill=fill, font=font)

def circlePos(x, y, size, margin):
	m = margin/2
	return (x*size+m, y*size+m, (x+1)*size-m, (y+1)*size-m)

def printWorld(env, state, valueTable, policyTable, path, policyNumber=False):
	img = Image.new(mode, size)
	draw = ImageDraw.Draw(img)
	
	width = size[0] // env.width
	height = size[1] // env.height
	
	if policyNumber:
		policyFunc = drawTextPolicy
	else:
		policyFunc = lambda draw, loc, fill, text, left=0, right=0, top=0, bottom=0:draw.ellipse(loc,fill=fill)
	
	draw.rectangle((0,0,size[0],size[1]), fill=(255,255,255))
	
	for i in range(0, size[0], width):
		draw.line((i, 0, i, size[1]), fill=(0,0,0))
	
	for i in range(0, size[1], height):
		draw.line((0, i, size[0], i), fill=(0,0,0))
	
	for (x,y), reward in env.rewards:
		color = (255, max(255*reward,0), max(-255*reward,0))
		draw.ellipse(circlePos(x, y, width, 30), fill=color)
		draw.text((x*width, y*height), "R:%d"%reward, fill=(0,0,0))
	
	draw.ellipse(circlePos(state[0], state[1], width, 30), fill=(255,0,0))
	
	for y, values in enumerate(valueTable):
		for x, v in enumerate(values):
			text = "%0.2f"%v
			textsize = draw.textsize(text)
			pos = ((x+1)*width-textsize[0], (y+1)*height-textsize[1])
			draw.text(pos, text, fill=(0,0,0))
	
	for y, policies in enumerate(policyTable):
		for x, p in enumerate(policies):
			if not (p or policyNumber):
				continue
			top, left, bott, right = p
			if top: policyFunc(draw, circlePos(x,y-0.4, width, width-4), fill=(0,0,0), text=top, top=True)
			if left: policyFunc(draw, circlePos(x-0.4,y, width, width-4), fill=(0,0,0), text=left, left=True)
			if bott: policyFunc(draw, circlePos(x,y+0.4, width, width-4), fill=(0,0,0), text=bott, bottom=True)
			if right: policyFunc(draw, circlePos(x+0.4,y, width, width-4), fill=(0,0,0), text=right, right=True)
			
	img.save(path)