
class Environment:
	def __init__(self, width, height):
		self.height = height
		self.width = width
		
		self.states = [[0.0]* width for i in range(height)]
		self.stateLists = [(h, w) for h in range(height) for w in range(width)]
		self.possibleActions = [0,1,2,3]

		self.rewards = []
		self.rewardDict = {i:[0] for i in self.stateLists}
	### actions
	# 0=top, 1=left, 2=bottom, 3=right
	
	def getAllStates(self):
		return self.stateLists
	
	def getStateAfterAction(self, state, action):
		y, x = state
		if action%2 == 0:
			y += action-1
		else:
			x += action-2

		if 0 <= x < self.width and 0 <= y < self.height:
			return (y, x)
		else:
			return state

	def setReward(self, state, reward):
		state = (int(state[0]), int(state[1]))
		self.rewards.append((state, reward))
		
		### should this be list??? or just 1???
		self.rewardDict[state].append(reward)

	def getReward(self, state):
		state = (int(state[0]), int(state[1]))
		return self.rewardDict[state][-1]