import random
from .env import Environment

class PolicyIteration:
	def __init__(self, env):
		self.env = env
		self.valueTable = [[0.00]*env.width for _ in range(env.height)]
		self.policyTable = [[[0.25,0.25,0.25,0.25]]*env.width for _ in range(env.height)]
		
		self.policyTable[2][2] = []
		self.discountFactor = 0.9
		
	
	def policyEvaluation(self):
		nextValueTable = [[0.00]*self.env.width for _ in range(self.env.height)]
		
		for state in self.env.getAllStates():
			value = 0.0
			if state == (2,2):
				nextValueTable[2][2] = 0.0
				continue
			
			for action in self.env.possibleActions:
				nextState = self.env.getStateAfterAction(state, action)
				reward = self.env.getReward(nextState)
				nextValue = self.getValue(nextState)
				value += self.getPolicy(state)[action] * (reward + self.discountFactor*nextValue)
			nextValueTable[state[0]][state[1]] = round(value, 2)

		self.valueTable = nextValueTable
	
	def policyImprovement(self):
		nextPolicy = self.policyTable
		for state in self.env.getAllStates():
			if state == (2,2):
				continue
			value = -99999
			maxIndex = []
			result = [0.0, 0.0, 0.0, 0.0]
			
			for index, action in enumerate(self.env.possibleActions):
				nextState = self.env.getStateAfterAction(state, action)
				reward = self.env.getReward(nextState)
				nextValue = self.getValue(nextState)
				temp = reward + self.discountFactor*nextValue
				
				if value == temp:
					maxIndex.append(index)
				elif value < temp:
					value = temp
					maxIndex.clear()
					maxIndex.append(index)
			
			prob = 1/len(maxIndex)
			for index in maxIndex:
				result[index] = prob
			nextPolicy[state[0]][state[1]] = result
		self.policyTable = nextPolicy
	
	def getAction(self, state):
		randomPick = random.randranger(100)/100
		policy = self.getPolicy(state)
		policySum = 0.0
		
		for index, value in enumerate(policy):
			policySum += value
			if randomPick < policySum:
				return index
	
	def getPolicy(self, state):
		if state == (2,2):
			return (0,0)
		return self.policyTable[state[0]][state[1]]
	
	def getValue(self, state):
		return round(self.valueTable[state[0]][state[1]], 2)
